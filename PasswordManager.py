# Create by gravifoxita #

# Import necessary modules
import hashlib
import os

# Define class for password manager
class PasswordManager:
    # Initialize with empty dictionary to store passwords
    def __init__(self):
        self.passwords = {}

    # Method to add new password to dictionary
    def add_password(self, website, username, password):
        # Hash password for security
        hashed_password = hashlib.sha256(password.encode('utf-8') + os.urandom(32)).hexdigest()
        # Add website and username as keys and hashed password as value
        self.passwords<(website, username)> = hashed_password

    # Method to get password from dictionary
    def get_password(self, website, username):
        # If website and username combination exists in dictionary
        if (website, username) in self.passwords:
            # Return hashed password
            return self.passwords<(website, username)>
        else:
            # Return error message
            return "Error: Website and/or username not found"

    # Method to delete password from dictionary
    def delete_password(self, website, username):
        # If website and username combination exists in dictionary
        if (website, username) in self.passwords:
            # Remove key-value pair from dictionary
            del self.passwords<(website, username)>
        else:
            # Return error message
            print("Error: Website and/or username not found")